﻿function log(x) {
    $.writeln(x);
};

function ObjPeek(obj, x) {
    switch(x) {
      case "p": 
      var y = obj.reflect.properties;       
      break;

      case "m":
      var y = obj.reflect.methods;       
      break;

      default: log('Please only use letters\n "p" : properties\n or "m" : methods'); 
      break;
    }    
    if (y != undefined) {
        for (var i in y) {
                log(y[i]);
        }
    }
};


function LogChildrenTree(obj, indt) {
    var children = obj.children;    
    var indent = indt;
    switch(children) {
      case undefined:
        break;
      default:       
        if (children.length > 0 && children.length != undefined) {
            indent += ' ';        
            for (var i = 0; i < children.length; i++) {
                try {                
                log(indent + children[i].properties.name);
                }
                catch(e){};
                LogChildrenTree(children[i], indent);
            }      
        };         
    }
};

