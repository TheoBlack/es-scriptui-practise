﻿#include "TestingTools.jsx"

var Dialog;

var DebugJawsDB = {
	Projects:{
		Items:[
		{Name:'Project 1', Id:1, Documents:[{Name:'Doc1',Id:1},{Name:'Doc2',Id:2},{Name:'Doc3',Id:3}]},
		{Name:'Project 2', Id:2, Documents:[{Name:'Doc4',Id:4},{Name:'Doc5',Id:5}]},
		{Name:'Project 1', Id:3, Documents:[{Name:'Doc11',Id:11},{Name:'Doc21',Id:21},{Name:'Doc31',Id:31}]},
		],
		Hash:{
			'1':{Name:'Project 1', Id:1, Documents:[{Name:'Doc1',Id:1},{Name:'Doc2',Id:2},{Name:'Doc3',Id:3}]},
			'2':{Name:'Project 2', Id:2, Documents:[{Name:'Doc4',Id:4},{Name:'Doc5',Id:5}]}
		}
	}
};


function Main() {

// TODO: Create Dialog
    CreateDialog();
// TODO: Show Dialog
    //ShowDialog();
// TODO: Log user selecions
    ObjPeek(Dialog, 'p');
    log("===================================");
    LogChildrenTree(Dialog, '');
    ShowDialog(Dialog);

}


function CreateDialog () {

    //TODO: Create Dialog Window
    Dialog = new Window('palette', 'Wordbee - InDesign');
    with ( Dialog ) {
        orientation = 'row';
        alignChildren = 'top';
        with ( add('group', undefined, {name: 'LeftColumn'})) {
            with ( Dialog.ProjectsPanel = add('panel', undefined, 'Projects')){                        
                name = 'ProjectsPanel';
                spacing = 10;
                with ( Dialog.ProjectsList = add('listbox', undefined)) {
                    name = 'ProjectsList';
                    minimumSize.height = 500;
                    minimumSize.width = 150;
                    active = true;
                    selection = 0;
                }
            }
        }
        with ( add('group', undefined, {name: 'RightColumn'})) {
            orientation = 'column';                  
            with ( Dialog.DocumentsPanel = add('panel', undefined, "Documents")) {
                name = 'DocumentsPanel';            
                with ( Dialog.DocumentsList = add('listbox', undefined) ) {
                    minimumSize.width = 250;
                    minimumSize.height = 150;
                    name = 'DocumentsList';
                    spacing = 10;
                }
            }
            with ( LanguagePanel = add('panel', undefined, "Language")) {
                name = 'LanguagePanel';            
                with ( Dialog.LanguageList = add('dropdownlist', undefined) ) {
                    minimumSize.width = 250;
                    minimumSize.height = 150;
                    name = 'LanguageList';
                    spacing = 10;
                }
                with (languageButtonRow = add('group', undefined)) {
                    orientation = 'row';
                    addLanguageButton = add('button', undefined, "Add Language");
                    removeLanguageButton = add('button', undefined, "Remove Language");

                }
            }
        }
    }
    FillProjectsData();  

    //TODO: Add Documents List
    //TODO: Add Lang Panel
        //TODO: Add Dropdown
    //TODO: Add Ok-Cancel
   
};
function FillProjectsData(){
	for (var i=0; i < DebugJawsDB.Projects.Items.length; i++){
		var project = DebugJawsDB.Projects.Items[i];
		Dialog.ProjectsList.add("item", project.Name, {project: project});
	}
	// Dialog.ProjectsList.selection = 0; 
};
















function ShowDialog(dialog) {
    dialog.show();
};

Main();
