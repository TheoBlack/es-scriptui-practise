﻿//Monitoring - Will remove
#include TestingTools.jsx

var Dialog;

function Main() {
    CreateDialog();    
    log("=================================================");        
    ObjPeek(Dialog, 'p');
    LogChildren(Dialog, '');
    //ShowDialog();
    //GetDialogData();
};


function CreateDialog() {
    //TODO: Create Window
    Dialog = new Window('dialog', 'Test Dialog Window');
    with (Dialog) {
        orientation = 'column';
        alignChildren = 'column';
        // Row 1
        with ( dialogRow1 = add('group', undefined, {name:'Row 1'})) {
            orientation = 'row';
            alignChildren = 'top';
            // Radio Buttons
            with ( row1RadioPanel = add('panel', undefined, "R1 Radio Panel", {name: 'R1 Radio Panel'})) {                
                alignChildren = 'center';
                spacing = 13;
                size = [115, 110];  
                margins = 15;
                Dialog.myRadioBox1 = add('radiobutton', undefined, "RadBox1", {name:'RadioBox1'});
                Dialog.myRadioBox2 = add('radiobutton', undefined, "RadBox2", {name:'RadioBox2'});
                Dialog.myRadioBox3 = add('radiobutton', undefined, "RadBox3", {name:'RadioBox3'});
            }
            // Checkboxes
            with ( row1CheckboxPanel = add('panel', undefined,"R1 Checkbox Panel", {name: 'R1 Checkbox Panel'})) {                
                alignChildren = 'fill';
                spacing = 5;
                size = [115, 110];  
                margins = 15;
                Dialog.myCheckBox1 = add('checkbox', undefined, "CheckBox 1", {name:'CHBX1'});
                Dialog.myCheckBox2 = add('checkbox', undefined, "CheckBox 2", {name:'CHBX2'});
                Dialog.myCheckBox3 = add('checkbox', undefined, "CheckBox 3", {name:'CHBX3'});
                Dialog.myCheckBox4 = add('checkbox', undefined, "CheckBox 4", {name:'CHBX4'});
            }
        }
        // Bottom Row 
        with ( bottomRow = add('group')) {
            orientation = 'row';            
            with ( ok_cancelButtonsGroup = add('group')) {
                orientation = 'row';
                t = add("statictext", undefined, "text");
                okButton = add("button", undefined, "OK");
                cancelButton = add("button", undefined, "Cancel");
                
        //TODO: Create buttons with listeners
        //TODO: Create Active text for user input

        
            }
        }
    }
};



function ShowDialog() {
    Dialog.show();
};

function GetDialogData() {
    //TODO: Get all user input data and store in global vars
        //TODO: Declare said global vars
};


Main();
